import React, { PureComponent } from 'react'
import { TextField } from 'material-ui';
import './LoginForm.css'

export default class LoginForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
		const { name, value } = event.target

		this.setState({
			[name]: value
		})
	}

	render() {
		return (
			<div className="login-form">
				<form onSubmit={this.handleSubmit}>
					<TextField
						id="email"
						label="Email"
						name="email"
						type="email"
						value={this.state.email}
						onChange={this.handleChange}
						margin="normal"
					/>
					<br />
					<TextField
						id="password"
						label="Password"
						name="password"
						type="password"
						value={this.state.password}
						onChange={this.handleChange}
						margin="normal"
					/>
					<br />
					<button type="submit">Login</button>
				</form>
			</div>)
	}
}
