import React from 'react'
import { connect } from 'react-redux'
import { createTicket } from '../../actions/events'
import TicketForm from './TicketForm'

class CreateTicketFormContainer extends React.Component {
    state = {
        name: '',
        date: '',
        price: '',
        location:'',
        image:'' 
    }

    handleSubmit = (data) => {
        this.props.createTicket(data.name, data.price,data.date, data.location, data.image)
    }

    render() {
        return (<TicketForm onSubmit={this.handleSubmit}/>)
    }
}

export default connect(null, { createTicket })(CreateTicketFormContainer)