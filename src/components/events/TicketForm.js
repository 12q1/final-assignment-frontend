import React, { Component } from 'react'
import './TicketForm.css'
import Card, { CardActions } from 'material-ui/Card'
import { TextField } from 'material-ui';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import Typography from 'material-ui/Typography'



export default class TicketForm extends Component {

    state = {
        expanded: false,
        name: '',
        price: '',
        date: '',
        location: '',
        image: '',
        risk: 50,
    }


    handleSubmit = (e) => {
        e.preventDefault()
        this.props.onSubmit(this.state)
    }

    handleChange = (ticket) => {
        const { name, value } = ticket.target

        this.setState({
            [name]: value
        })
    }

    handleExpandClick = () => {
        this.setState(state => ({ expanded: !state.expanded }));
    };

    render() {
        console.log(this.state)

        return (
            <Card className="create-ticket-form">
                <CardActions className="Collapse" disableActionSpacing>
                    <Typography color="textSecondary">
                        Create a Ticket
                    </Typography>
                    <IconButton

                        className="Expand Button"
                        onClick={this.handleExpandClick}
                        aria-expanded={this.state.expanded}
                        aria-label="Show more"
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                    <form onSubmit={this.handleSubmit}>
                        <TextField
                            id="name"
                            label="Name"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />
                        <TextField
                            id="price"
                            label="Price"
                            name="price"
                            type="number"
                            value={this.state.price}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />

                        <button type="submit">Submit</button>
                    </form>
                </Collapse>
            </Card>

        )
    }
}
