import React, {PureComponent} from 'react'
import {getEvents, createEvent} from '../../actions/events'
import {getUsers} from '../../actions/users'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import Button from 'material-ui/Button'
import Paper from 'material-ui/Paper'
import Card, { CardActions, CardContent, CardMedia, CardHeader } from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import './EventsList.css'
import CreateEventFormContainer from './CreateEventFormContainer'

class EventsList extends PureComponent {

  componentWillMount() {
    if (this.props.authenticated) {
      if (this.props.events === null) this.props.getEvents()
      if (this.props.users === null) this.props.getUsers()
    }
  }

  renderEvent = (event) => {
    const {history} = this.props
    //console.log(this.props)

    return (
    <Card key={event.id} className="event-card">
      <CardHeader title={event.name}/>
      <CardMedia
        image={event.image}
        style={{maxHeight:100,paddingTop:'40%'}}
        
      />
      <CardContent>
        <Typography color="textSecondary">
          {event.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          onClick={() => history.push(`/events/${event.id}`)}
        >
          Details
        </Button>
      </CardActions>
    </Card>)
  }

  render() {
    const {events, users, authenticated } = this.props

    if (!authenticated) return (
			<Redirect to="/login" />
		)
   if (events === null || users === null) return null

    return (<Paper className="outer-paper">
      <CreateEventFormContainer/>
      <div>
        {events.map(event => this.renderEvent(event))}
      </div>
    </Paper>)
  }
}

const mapStateToProps = state => ({
  authenticated: state.currentUser !== null,
  users: state.users === null ? null : state.users,
  events: state.events === null ?
    null : Object.values(state.events).sort((a, b) => b.id - a.id)
})

export default connect(mapStateToProps, {getEvents, getUsers, createEvent})(EventsList)
