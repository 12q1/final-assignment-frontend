import React, { Component } from 'react'
import './EventForm.css'
import Card, { CardActions } from 'material-ui/Card'
import { TextField } from 'material-ui';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import Typography from 'material-ui/Typography'



export default class EventForm extends Component {

    state = {
        expanded: false,
        name: '',
        description: '',
        date: '',
        location: '',
        image: '',
    }


    handleSubmit = (e) => {
        e.preventDefault()
        this.props.onSubmit(this.state)
    }

    handleChange = (event) => {
        const { name, value } = event.target

        this.setState({
            [name]: value
        })
    }

    handleExpandClick = () => {
        this.setState(state => ({ expanded: !state.expanded }));
    };

    render() {
        console.log(this.state)

        return (
            <Card className="create-event-form">
                <CardActions className="Collapse" disableActionSpacing>
                    <Typography color="textSecondary">
                        Create an Event
                    </Typography>
                    <IconButton

                        className="Expand Button"
                        onClick={this.handleExpandClick}
                        aria-expanded={this.state.expanded}
                        aria-label="Show more"
                    >
                        <ExpandMoreIcon />
                    </IconButton>
                </CardActions>
                <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
                    <form onSubmit={this.handleSubmit}>
                        <TextField
                            id="name"
                            label="Name"
                            name="name"
                            value={this.state.name}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />
                        <TextField
                            id="description"
                            label="Description"
                            name="description"
                            value={this.state.description}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />
                        <TextField
                            id="date"
                            label="Date"
                            name="date"
                            value={this.state.date}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />

                        <TextField
                            id="location"
                            label="Location"
                            name="location"
                            value={this.state.location}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />

                        <TextField
                            id="image"
                            label="Image"
                            name="image"
                            value={this.state.image}
                            onChange={this.handleChange}
                            margin="normal"
                        />
                        <br />

                        <button type="submit">Submit</button>
                    </form>
                </Collapse>
            </Card>

        )
    }
}
