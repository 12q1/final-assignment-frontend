import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { getEvents, joinEvent, updateEvent } from '../../actions/events'
import { getUsers } from '../../actions/users'
import { userId } from '../../jwt'
import Paper from 'material-ui/Paper'
import './EventDetails.css'
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card'
import Typography from 'material-ui/Typography'
import Divider from '@material-ui/core/Divider';
import { CardHeader } from '@material-ui/core';
import TicketFormContainer from './TicketFormContainer';

class EventDetails extends PureComponent {

  componentWillMount() {
    if (this.props.authenticated) {
      if (this.props.event === null) this.props.getEvents()
      if (this.props.users === null) this.props.getUsers()
    }
  }

  checkPrice(price){
    let avgPrice = this.props.event.tickets.map(tickets=>parseInt(tickets.price)).reduce((acc,curr)=>acc+curr)/this.props.event.tickets.length
    //console.log(price/avgPrice)
    if(parseInt(price) === avgPrice) return 0 //if the price is the same as the average price, do nothing
    if(parseInt(price)>avgPrice) return -price/avgPrice //if the price is greater than the avgPrice reduce the % to the risk profile
    if(parseInt(price)<avgPrice) return price/avgPrice //if the price is lower than the avgPrice increase the % to the risk profile
  }

  checkUser(user){
    //console.log(this.props.users[user].tickets.length)
    if(this.props.users[user].tickets.length<=1) return 10 //if the user has less than 1 ticket increase risk profile by 10
    if(this.props.users[user].tickets.length>1) return 0 //if the user has more than 1 ticket do nothing
  }

  checkComments(comments){
    if(comments.length>3) return 5 //if a ticket has more than 3 comments increase risk profile by 5
    if(comments.length<=3) return 0 //if a ticket has less than 3 comments do nothing
  }

  checkTime(time){
    let tempDate = new Date(time) //convert ticket.created to a timestamp for some reason not being recognized normally
    if(9<= tempDate.getHours() && tempDate.getHours() <17) {return -10} //if time is greater than 9 or less than 17 deduct 10 from risk profile
    else {return 10} //else increase risk profile by 10
  }


  render() {
    const { event, users, authenticated } = this.props
    if (!authenticated) return (
      <Redirect to="/login" />
    )

    if (event === null || users === null) return 'Loading...'
    if (!event) return 'Not found'
    if (event.tickets == null) event.tickets=[]
    
    


    return (<Paper className="outer-paper">
      <Card key={event.id} className="event-card">
        <CardHeader title={event.name}/>
        <CardMedia
          image={event.image}
          style={{ maxHeight: 100, paddingTop: '40%' }}
        />
        <CardContent>
          <Typography color="textSecondary">
            {event.description}
          </Typography>
          
          <Typography color="textSecondary">
            <b>Location: </b> {event.location}
          </Typography>
          
          <Typography color="textSecondary">
            <b>Start:</b> {event.startDate}
          </Typography>
          
          <Typography color="textSecondary">
            <b>End:</b> {event.endDate}
          </Typography>
          
          <Typography color="textSecondary" >
            <b>Available Tickets</b>
          </Typography>
          
            <Divider/>
          <ul>
            {event.tickets.map(ticket => <li key={ticket.id}> <b>{ticket.name}</b> - Price: {ticket.price} - Risk: {ticket.risk+this.checkPrice(ticket.price)+this.checkUser(ticket.userId)+this.checkComments(ticket.comments)+this.checkTime(ticket.created)}%</li>)}
          </ul>


        </CardContent>
        <CardActions>
        <TicketFormContainer/>
        </CardActions>
      </Card>


    </Paper>)
  }
}

const mapStateToProps = (state, props) => ({
  authenticated: state.currentUser !== null,
  userId: state.currentUser && userId(state.currentUser.jwt),
  event: state.events && state.events[props.match.params.id],
  users: state.users
})

const mapDispatchToProps = {
  getEvents, getUsers, joinEvent, updateEvent
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails)
