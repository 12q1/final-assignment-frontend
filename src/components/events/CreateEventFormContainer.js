import React from 'react'
import { connect } from 'react-redux'
import { createEvent } from '../../actions/events'
import EventForm from './EventForm'

class CreateEventFormContainer extends React.Component {
    state = {
        name: '',
        date: '',
        description: '',
        location:'',
        image:''
    }

    handleSubmit = (data) => {
        this.props.createEvent(data.name, data.description,data.date, data.location, data.image)
    }

    render() {
        return (<EventForm onSubmit={this.handleSubmit}/>)
    }
}

export default connect(null, { createEvent })(CreateEventFormContainer)