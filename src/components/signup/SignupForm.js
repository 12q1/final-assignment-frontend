import React, {PureComponent} from 'react'
import { TextField } from 'material-ui';
import './SignupForm.css'

export default class SignupForm extends PureComponent {
	state = {}

	handleSubmit = (e) => {
		e.preventDefault()
		this.props.onSubmit(this.state)
	}

	handleChange = (event) => {
    const {name, value} = event.target

    this.setState({
      [name]: value
    })
  }

	render() {
		return (
      <div className="signup-form">
  			<form onSubmit={this.handleSubmit}>
				<TextField
						id="email"
						label="Email"
						name="email"
						type="email"
						value={this.state.email}
						onChange={this.handleChange}
						margin="normal"
					/>
					<br />
					<TextField
						id="password"
						label="Password"
						name="password"
						type="password"
						value={this.state.password}
						onChange={this.handleChange}
						margin="normal"
					/>
					<br />
					<TextField
						id="confirmpassword"
						label="Confirm Password"
						name="confirmPassword"
						type="password"
						value={this.state.confirmPassword}
						onChange={this.handleChange}
						margin="normal"
					/>
					<br />

  				{
  					this.state.password &&
  					this.state.confirmPassword &&
  					this.state.password !== this.state.confirmPassword &&
  					<p style={{color:'red'}}>The passwords do not match!</p>
  				}

  				<button type="submit">Sign up</button>
  			</form>
      </div>
		)
	}
}
