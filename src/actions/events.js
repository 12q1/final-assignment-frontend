import * as request from 'superagent'
import { baseUrl } from '../constants'
import { logout } from './users'
import { isExpired } from '../jwt'

export const ADD_EVENT = 'ADD_EVENT'
export const ADD_TICKET = 'ADD_TICKET'
export const UPDATE_EVENT = 'UPDATE_EVENT'
export const UPDATE_EVENTS = 'UPDATE_EVENTS'
export const JOIN_EVENT_SUCCESS = 'JOIN_EVENT_SUCCESS'
export const UPDATE_EVENT_SUCCESS = 'UPDATE_EVENT_SUCCESS'

const updateEvents = events => ({
  type: UPDATE_EVENTS,
  payload: events
})

const addEvent = event => ({
  type: ADD_EVENT,
  payload: event
})

const addTicket = ticket => ({
  type: ADD_TICKET,
  payload: ticket
})

const updateEventSuccess = () => ({
  type: UPDATE_EVENT_SUCCESS
})

const joinEventSuccess = () => ({
  type: JOIN_EVENT_SUCCESS
})


export const getEvents = () => (dispatch, getState) => {
  const state = getState()
  if (!state.currentUser) return null
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  request
    .get(`${baseUrl}/events`)
    .set('Authorization', `Bearer ${jwt}`)
    .then(result => dispatch(updateEvents(result.body.events)))
    .catch(err => console.error(err))
}

export const joinEvent = (eventId) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  request
    .post(`${baseUrl}/events/${eventId}/players`)
    .set('Authorization', `Bearer ${jwt}`)
    .then(_ => dispatch(joinEventSuccess()))
    .catch(err => console.error(err))
}

export const createEvent = (name, description, date, location, image) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt
  console.log(image)
  if (isExpired(jwt)) return dispatch(logout())

  request
    .post(`${baseUrl}/events`)
    .set('Authorization', `Bearer ${jwt}`)
    .send({ name, description, date, location, image })
    .then(result => dispatch(addEvent(result.body)))
    .catch(err => console.error(err))
}

export const createTicket = (name, price, risk) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt
  console.log(risk)
  if (isExpired(jwt)) return dispatch(logout())

  request
    .post(`${baseUrl}/tickets`)
    .set('Authorization', `Bearer ${jwt}`)
    .send({ name, price, risk })
    .then(result => dispatch(addTicket(result.body)))
    .catch(err => console.error(err))
}

export const updateEvent = (eventId, board) => (dispatch, getState) => {
  const state = getState()
  const jwt = state.currentUser.jwt

  if (isExpired(jwt)) return dispatch(logout())

  request
    .patch(`${baseUrl}/events/${eventId}`)
    .set('Authorization', `Bearer ${jwt}`)
    .send({ board })
    .then(_ => dispatch(updateEventSuccess()))
    .catch(err => console.error(err))
}
