# Final Assignment Week 8 - Front end client

This application uses React, Redux, SocketIO, Material UI, and additional libraries for middleware and routing.

It was built using many parts borrowed from this starter-pack: https://github.com/Codaisseur/game-starter-b15

It communicates with the backend API hosted on a separate repository.

The application can be started by navigating to the root directory and entering 'yarn start'.

When running the application uses localhost:3000 but will take environment variables if they are available.

The user signs up or logs into the application and is given a JWT token stored in the local storage.

Once authorized the user is allowed to continue to the '/events' and and '/tickets' endpoints.

Built by Thomas Ham for Codaisseur
